# --------------------------
# General/Common parameters
# --------------------------
variable "s3_logs_bucket_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# ---------------------
# S3 bucket parameters
# ---------------------
variable "bucket_name" {
  description = "The name of the bucket."
  # If omitted, Terraform will assign a random, unique name.
  type        = string
  default     = ""
}

variable "bucket_prefix" {
  description = "Creates a unique bucket name beginning with the specified prefix. Conflicts with `bucket`."
  type        = string
  default     = ""
}

variable "bucket_tags" {
  description = "A mapping of tags to assign to the bucket."
  type        = map(string)
  default     = {}
}

variable "acl" {
  description = "The canned ACL to apply. We recommend log-delivery-write for compatibility with AWS services."
  type        = string
  default     = "log-delivery-write"
}

variable "policy" {
  description = "A valid bucket policy JSON document."
  # Note(!): that if the policy document is not specific enough (but still valid), Terraform may view the policy as
  # constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version
  # of the policy
  type        = string
  default     = ""
}

variable "force_destroy" {
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable"
  type        = bool
  default     = false
}

variable "region" {
  description = "If specified, the AWS region this bucket should reside in. Otherwise, the region used by the callee"
  type        = string
  default     = null
}

variable "versioning_enabled" {
  description = "A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket"
  type        = bool
  default     = false
}

variable "sse_algorithm" {
  description = "The server-side encryption algorithm to use."
  # Valid values are:
  #   * AES256
  #   * aws:kms
  type        = string
  default     = "AES256"
}
variable "kms_master_key_arn" {
  description = "The AWS KMS master key ARN used for the SSE-KMS encryption."
  # This can only be used when you set the value of `sse_algorithm` as `aws:kms`.
  # The default aws/s3 AWS KMS master key is used if this element is absent while the `sse_algorithm` is `aws:kms`.
  type        = string
  default     = ""
}

variable "lifecycle_rule_enabled" {
  description = "Enable lifecycle events on this bucket."
  type        = bool
  default     = true
}
variable "lifecycle_prefix" {
  description = "Prefix filter. Used to manage object lifecycle events."
  type        = string
  default     = ""
}
variable "lifecycle_tags" {
  description = "Tags filter. Used to manage object lifecycle events."
  type        = map(string)
  default     = {}
}

variable "noncurrent_version_expiration_days" {
  description = "Specifies when noncurrent object versions expire."
  default     = 90
}

variable "noncurrent_version_transition_days" {
  description = "Specifies when noncurrent object versions transitions."
  default     = 30
}

variable "standard_transition_days" {
  description = "Number of days to persist in the standard storage tier before moving to the infrequent access tier."
  default     = 30
}

variable "glacier_transition_days" {
  description = "Number of days after which to move the data to the glacier storage tier."
  default     = 60
}

variable "expiration_days" {
  description = "Number of days after which to expunge the objects."
  default     = 90
}
