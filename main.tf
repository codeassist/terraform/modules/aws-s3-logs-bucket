locals {
  is_named_bucket               = length(var.bucket_name) > 0 ? true : false
  is_prefixed_bucket            = length(var.bucket_prefix) > 0 ? true : false
  enabled                       = (var.s3_logs_bucket_module_enabled && (local.is_named_bucket || local.is_prefixed_bucket)) ? true : false

  tags                          = merge(
    var.bucket_tags,
    {
      terraform = "true"
    },
  )
}


resource "random_id" "lifecycle_id" {
# The resource `random_id` generates random numbers that are intended to be used as unique identifiers for other
# resources.
  count = (local.enabled && var.lifecycle_rule_enabled) ? 1 : 0

  # (Required) The number of random bytes to produce. The minimum value is 1, which produces eight bits of randomness.
  byte_length = 8

  # (Optional) Arbitrary map of values that, when changed, will trigger a new id to be generated. See the main provider
  # documentation for more information:
  #   * https://www.terraform.io/docs/providers/random/index.html
  keepers = {
    bucket = local.is_named_bucket ? var.bucket_name : var.bucket_prefix
  }
}


resource "aws_s3_bucket" "named_bucket" {
# Provides a S3 bucket resource.
  count = (local.enabled && local.is_named_bucket) ? 1 : 0

  # (Optional, Forces new resource) The name of the bucket. If omitted, Terraform will assign a random, unique name.
  bucket        = var.bucket_name
  # (Optional) A mapping of tags to assign to the bucket.
  tags          = merge(
    local.tags,
    map("Name", var.bucket_name)
  )
  # (Optional) The canned ACL to apply.
  acl           = var.acl
  # (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but
  # still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make
  # sure you use the verbose/specific version of the policy.
  policy        = var.policy
  # (Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be
  # destroyed without error. These objects are not recoverable.
  force_destroy = var.force_destroy
  # (Optional) If specified, the AWS region this bucket should reside in. Otherwise, the region used by the callee.
  region        = var.region

  # A state of versioning object:
  versioning {
    # (Optional) Enable versioning. Once you version-enable a bucket, it can never return to an unversioned state.
    # You can, however, suspend versioning on that bucket.
    enabled = var.versioning_enabled
  }

  # (Optional) A configuration of server-side encryption, see more at:
  #   * https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  #   * https://www.terraform.io/docs/providers/aws/r/s3_bucket.html#enable-default-server-side-encryption
  server_side_encryption_configuration {
    # (required) A single object for server-side encryption by default configuration.
    rule {
      # (required) A single object for setting server-side encryption by default.
      apply_server_side_encryption_by_default {
        # (required) The server-side encryption algorithm to use. Valid values are `AES256` and `aws:kms`
        sse_algorithm     = var.sse_algorithm

        # (optional) The AWS KMS master key ID used for the SSE-KMS encryption. This can only be used when you set
        # the value of `sse_algorithm` as `aws:kms`. The default `aws/s3` AWS KMS master key is used if this element
        # is absent while the `sse_algorithm` is `aws:kms`.
        kms_master_key_id = var.kms_master_key_arn
      }
    }
  }

  # (Optional) A configuration of object lifecycle management, see at:
  #   * https://docs.aws.amazon.com/en_us/AmazonS3/latest/dev/object-lifecycle-mgmt.html
  lifecycle_rule {
    # (Required) Specifies lifecycle rule status.
    enabled = var.lifecycle_rule_enabled

    # (Optional) Unique identifier for the rule.
    id      = format("%s-lifecycle-%s", var.bucket_name, join("", random_id.lifecycle_id.*.dec))
    # (Optional) Object key prefix identifying one or more objects to which the rule applies.
    prefix = var.lifecycle_prefix
    # (Optional) Specifies object tags key and value.
    tags   = merge(
      local.tags,
      var.lifecycle_tags,
    )

# At least one of must be specified:
#   * expiration,
#   * transition,
#   * noncurrent_version_expiration,
#   * noncurrent_version_transition

    # (Optional) Specifies when noncurrent object versions expire.
    noncurrent_version_expiration {
      # (Optional) Specifies the number of days after object creation when the specific rule action takes effect.
      days = var.noncurrent_version_expiration_days
    }

    # (Optional) Specifies when noncurrent object versions transitions.
    noncurrent_version_transition {
      # (Optional) Specifies the number of days after object creation when the specific rule action takes effect.
      days          = var.noncurrent_version_transition_days
      # (Required) Specifies the Amazon S3 storage class to which you want the object to transition. Can be:
      #   * ONEZONE_IA,
      #   * STANDARD_IA,
      #   * INTELLIGENT_TIERING,
      #   * GLACIER,
      #   * DEEP_ARCHIVE.
      storage_class = "GLACIER"
    }

# Any `expiration` and/or `transition` objects supports the same arguments (see above).

    # (Optional) Specifies a period in the object's transitions.
    transition {
      days          = var.standard_transition_days
      storage_class = "STANDARD_IA"
    }

    # (Optional) Specifies a period in the object's transitions.
    transition {
      days          = var.glacier_transition_days
      storage_class = "GLACIER"
    }

    # (Optional) Specifies a period in the object's expire.
    expiration {
      days = var.expiration_days
    }
  }
}

resource "aws_s3_bucket" "prefixed_bucket" {
# Provides a S3 bucket resource.
  count = (local.enabled && !local.is_named_bucket && local.is_prefixed_bucket) ? 1 : 0

  # (Optional, Forces new resource) Creates a unique bucket name beginning with the specified prefix.
  # Conflicts with `bucket`.
  bucket_prefix = var.bucket_prefix
  # (Optional) A mapping of tags to assign to the bucket.
  tags          = merge(
    local.tags,
    map("Prefix", var.bucket_prefix)
  )
  # (Optional) The canned ACL to apply.
  acl           = var.acl
  # (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but
  # still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make
  # sure you use the verbose/specific version of the policy.
  policy        = var.policy
  # (Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be
  # destroyed without error. These objects are not recoverable.
  force_destroy = var.force_destroy
  # (Optional) If specified, the AWS region this bucket should reside in. Otherwise, the region used by the callee.
  region        = var.region

  # A state of versioning object:
  versioning {
    # (Optional) Enable versioning. Once you version-enable a bucket, it can never return to an unversioned state.
    # You can, however, suspend versioning on that bucket.
    enabled = var.versioning_enabled
  }

  # (Optional) A configuration of server-side encryption, see more at:
  #   * https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  #   * https://www.terraform.io/docs/providers/aws/r/s3_bucket.html#enable-default-server-side-encryption
  server_side_encryption_configuration {
    # (required) A single object for server-side encryption by default configuration.
    rule {
      # (required) A single object for setting server-side encryption by default.
      apply_server_side_encryption_by_default {
        # (required) The server-side encryption algorithm to use. Valid values are `AES256` and `aws:kms`
        sse_algorithm     = var.sse_algorithm

        # (optional) The AWS KMS master key ID used for the SSE-KMS encryption. This can only be used when you set
        # the value of `sse_algorithm` as `aws:kms`. The default `aws/s3` AWS KMS master key is used if this element
        # is absent while the `sse_algorithm` is `aws:kms`.
        kms_master_key_id = var.kms_master_key_arn
      }
    }
  }

  # (Optional) A configuration of object lifecycle management, see at:
  #   * https://docs.aws.amazon.com/en_us/AmazonS3/latest/dev/object-lifecycle-mgmt.html
  lifecycle_rule {
    # (Required) Specifies lifecycle rule status.
    enabled = var.lifecycle_rule_enabled

    # (Optional) Unique identifier for the rule.
    id      = format("%s-lifecycle-%s", var.bucket_name, join("", random_id.lifecycle_id.*.dec))
    # (Optional) Object key prefix identifying one or more objects to which the rule applies.
    prefix = var.lifecycle_prefix
    # (Optional) Specifies object tags key and value.
    tags   = merge(
      local.tags,
      var.lifecycle_tags,
    )

# At least one of must be specified:
#   * expiration,
#   * transition,
#   * noncurrent_version_expiration,
#   * noncurrent_version_transition

    # (Optional) Specifies when noncurrent object versions expire.
    noncurrent_version_expiration {
      # (Optional) Specifies the number of days after object creation when the specific rule action takes effect.
      days = var.noncurrent_version_expiration_days
    }

    # (Optional) Specifies when noncurrent object versions transitions.
    noncurrent_version_transition {
      # (Optional) Specifies the number of days after object creation when the specific rule action takes effect.
      days          = var.noncurrent_version_transition_days
      # (Required) Specifies the Amazon S3 storage class to which you want the object to transition. Can be:
      #   * ONEZONE_IA,
      #   * STANDARD_IA,
      #   * INTELLIGENT_TIERING,
      #   * GLACIER,
      #   * DEEP_ARCHIVE.
      storage_class = "GLACIER"
    }

# Any `expiration` and/or `transition` objects supports the same arguments (see above).

    # (Optional) Specifies a period in the object's transitions.
    transition {
      days          = var.standard_transition_days
      storage_class = "STANDARD_IA"
    }

    # (Optional) Specifies a period in the object's transitions.
    transition {
      days          = var.glacier_transition_days
      storage_class = "GLACIER"
    }

    # (Optional) Specifies a period in the object's expire.
    expiration {
      days = var.expiration_days
    }
  }
}
