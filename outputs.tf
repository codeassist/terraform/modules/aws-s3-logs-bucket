locals {
  output_bucket = local.is_named_bucket ? (length(aws_s3_bucket.named_bucket) > 0 ? jsonencode(aws_s3_bucket.named_bucket[0]) : jsonencode({})) : (length(aws_s3_bucket.prefixed_bucket) > 0 ? jsonencode(aws_s3_bucket.prefixed_bucket[0]): jsonencode({}))
}

output "bucket_id" {
  description = "The name of the bucket."
  value       = local.enabled ? lookup(jsondecode(local.output_bucket), "id", "") : ""
}

output "bucket_arn" {
  description = "The ARN of the bucket."
  value       = local.enabled ? lookup(jsondecode(local.output_bucket), "arn", "") : ""
}

output "bucket_domain_name" {
  description = "The bucket domain name (FQDN)."
  value       = local.enabled ? lookup(jsondecode(local.output_bucket), "bucket_domain_name", "") : ""
}

output "bucket_region" {
  description = "The AWS region this bucket resides in."
  value       = local.enabled ? lookup(jsondecode(local.output_bucket), "region", "") : ""
}
